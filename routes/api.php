<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::post('register', [UserController::class, 'register']);
Route::post('login', 'UserController@authenticate');


Route::group(['middleware' => ['jwt.verify']], function() {

});
